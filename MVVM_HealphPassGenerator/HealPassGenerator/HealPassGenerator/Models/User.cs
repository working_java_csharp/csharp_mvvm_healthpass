﻿using ConcurrentPriorityQueue.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealPassGenerator.Models
{
    public class User : IComparable<User>, IHavePriority<int>
    {
        public int Id { get; set; } // Utiliser l'unicité des clés du dictionnaire
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string BirthDay { get; set; }
        public HealphPass HealphPass { get; set; }
        public int Priority { get {
                priority = getPriority();
                return priority;
            } set {
                priority = value;
            } }

        public bool isGood()
        {
            return HealphPass != default
                   && !String.IsNullOrWhiteSpace(Firstname)
                   && !String.IsNullOrWhiteSpace(Lastname)
                   && !String.IsNullOrWhiteSpace(BirthDay);
        }

        private int priority;
        public int getPriority() =>  HealphPass.status == CovidStatus.Positif ? 0 : 1; 

        public int CompareTo(User other)
        {
            if (!isGood() || (!isGood() && other.isGood())) return -1;
            if (!isGood() && !other.isGood()) return 0;
            if (!other.isGood() || other.HealphPass.status < HealphPass.status) return 1;
            if (other.HealphPass.status == HealphPass.status)
            {
               var othDate =  DateTime.Parse(other.HealphPass.RequestDate).Millisecond;
                var myDate = DateTime.Parse(HealphPass.RequestDate).Millisecond;
                if (othDate < myDate) return 1;
                if (othDate == myDate) return 0;
            }
            return -1;
        }

      /*  public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (ReferenceEquals(obj, null))
            {
                return false;
            }

            return false;
        }*/

        public override int GetHashCode()
        {
            return Firstname.GetHashCode()^Lastname.GetHashCode()^BirthDay.GetHashCode();
        }

        public static bool operator ==(User left, User right)
        {
            if (ReferenceEquals(left, null))
            {
                return ReferenceEquals(right, null);
            }

            return left.Equals(right);
        }

        public static bool operator !=(User left, User right)
        {
            return !(left == right);
        }

        public static bool operator <(User left, User right)
        {
            return ReferenceEquals(left, null) ? !ReferenceEquals(right, null) : left.CompareTo(right) < 0;
        }

        public static bool operator <=(User left, User right)
        {
            return ReferenceEquals(left, null) || left.CompareTo(right) <= 0;
        }

        public static bool operator >(User left, User right)
        {
            return !ReferenceEquals(left, null) && left.CompareTo(right) > 0;
        }

        public static bool operator >=(User left, User right)
        {
            return ReferenceEquals(left, null) ? ReferenceEquals(right, null) : left.CompareTo(right) >= 0;
        }
    }
}
