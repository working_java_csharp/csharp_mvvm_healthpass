﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealPassGenerator.Models
{
    public class HealphPass
    {
        public string RequestDate { get; set; }
        public string GeneratetDate { get; set; }
        public CovidStatus status { get; set; }
        public DownloadState downloadState { get; set; }
        public string DownloadStateColor { get => getDownloadColor(); set => DownloadStateColor = value; }

        private string getDownloadColor()
        {
            switch (downloadState)
            {
                case DownloadState.Download: return "Green";
                case DownloadState.Progress: return "Orange";
                case DownloadState.None:
                default: return "Red";
            }
        }
    }

    public enum DownloadState
    {
        Download,
        Progress,
        None
    }
}
