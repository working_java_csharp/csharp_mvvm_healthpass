﻿using HealPassGenerator.Models;
using HealPassGenerator.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealPassGenerator.Manager
{
    public interface IUserManager
    {
        public void generateHPPdf(User user, Action<int> action);
        void generateAllAsyn(Action<(int, User)> action);
        Task generateAllAsyncPC(Action<(int, User)> action);
        public bool Create(User user);
        public (int success, int error) Create(List<User> users);
        public List<User> Create(int size = 100);
        public User find(string Firstname, string Lastname, string BirthDay, int id);
        public User find(User user);
        public List<User> find(List<User> users);
        public int Count();
        public bool delete(string Firstname, string Lastname, string BirthDay, int id);
        public bool delete(User user);
        public (int success, int error) delete(List<User> user);
    }
}
