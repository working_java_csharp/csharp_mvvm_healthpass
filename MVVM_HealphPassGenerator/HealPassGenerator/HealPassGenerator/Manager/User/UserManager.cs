﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using HealPassGenerator.Models;
using Microsoft.Extensions.Logging;
using HealPassGenerator.Helpers;
using System.Diagnostics;
using System.Threading;
using ConcurrentPriorityQueue.Core;
using System.Collections.ObjectModel;
using HealPassGenerator.Views;
using System.Windows;

namespace HealPassGenerator.Manager
{
    public class UserManager : IUserManager
    {
        #region Field
        private readonly ConcurrentDictionary<string,User> users;
        private readonly HealphPassPDFGeHelper healphPassPDFGeHelper;
        private readonly ProducerConsumer producerConsumer;
        //  private readonly ILogger<UserManager> logger = Lo;
        #endregion Field
        #region Constructor
        public UserManager()
        {
            users = new ConcurrentDictionary<string, User>();
            healphPassPDFGeHelper = new HealphPassPDFGeHelper();
            producerConsumer = new ProducerConsumer();
        }
        #endregion Constructor
        #region Create Methods

       
        
        public void generateHPPdf(User user, Action<int> action)
        {
            int progess = 0;
            action.Invoke(0);
            user.HealphPass.GeneratetDate = DateTime.Now.ToString();
            action.Invoke(7);
            healphPassPDFGeHelper.generate(user, ref progess);
            action.Invoke(progess);
            action.Invoke(2);
        }

        private int generateHPPdf(User user)
        {
            int progess = 0;
            user.HealphPass.GeneratetDate = DateTime.Now.ToString();
            progess+= 7;
            healphPassPDFGeHelper.generate(user, ref progess);
            progess+= 2; 
            return progess;
        }

        public void generateAllAsyn(Action<(int, User)> action)
        {
            var concurrentPriorityQueue = new ConcurrentPriorityQueue<IHavePriority<int>,int>();
            this.users.Values.ToList().ForEach(u => {
                concurrentPriorityQueue.Enqueue(u);
            });
            int size = users.Count;
            //action.Invoke(0);
            Parallel.For(0, concurrentPriorityQueue.Count, u =>
            {
                var us = concurrentPriorityQueue.Dequeue().Value;
                if(us is User)
                {
                    var user = us as User; // Fragile en cas de changement de type 'User'
                                           // generateHPPdf(user,windowHP,Users,Progess);
                    action.Invoke((generateHPPdf(user)/ size, user));
                }
                
                
            });
        }
       

        public Task generateAllAsyncPC(Action<(int, User)> action)
        {
            //action.Invoke(0);
            producerConsumer.produce(users.Values.ToList()).Wait();
            return Task.WhenAll(
               
                new Task[]
                {
                    producerConsumer.consume(user =>
                    {
                       action.Invoke((generateHPPdf(user)/users.Count, user));
                    })
                }) ;

        }

        public bool Create(User user)
        {
            if (user == default || !user.isGood()) return false;
            string key = UserHelper.generateKey(user);
            return users.TryAdd<string, User>(key, user);
        }

        public List<User> Create(int size = 100)
        {
            var result = new List<User>();
            for(int i =0; i< size; i++)
            {
                var user = UserFactory.Create();
                if (Create(user)) result.Add(user);
            }
           // var users = UserFactory.CreateList(size);
            //Create(users);
            return result;
        }

        public (int success, int error) Create(List<User> users)
        {
            
            int success = 0;
            int error = 0;
            users?.ForEach(u =>
            {
                if (Create(u)) success++; else error++;
            });
            return (success, error);
        }
        #endregion Create Methods
        #region Count Methods
        public int Count() => users.Count();
        #endregion Count Methods
        #region Find Methods

        public User find(string Firstname, string Lastname, string BirthDay, int id)
        {
            if (String.IsNullOrWhiteSpace(Firstname) || String.IsNullOrWhiteSpace(BirthDay) || String.IsNullOrWhiteSpace(Lastname))
                return default;
            users.TryGetValue(UserHelper.generateKey(Firstname,Lastname, BirthDay, id), out User user);
            return user;
        }

        public User find(User user)
        => user == default ? default :
            find(user.Firstname, user.Lastname, user.BirthDay, user.Id);


        public List<User> find(List<User> users)
            => (users == default || !users.Any() )? users : users.Select(u => find(u)).Where(u => u != default).ToList();

        #endregion Find Methods
        #region Delete Methods
        public bool delete(string Firstname, string Lastname, string BirthDay, int id)
        {
            if (String.IsNullOrWhiteSpace(Firstname) || String.IsNullOrWhiteSpace(BirthDay) || String.IsNullOrWhiteSpace(Lastname))
                return false;
            return users.TryRemove(UserHelper.generateKey(Firstname, Lastname, BirthDay, id), out User user);
        }
        
        public bool delete(User user)
        => user == default ? false :
            delete(user.Firstname, user.Lastname, user.BirthDay, user.Id);


        public (int success, int error) delete(List<User> users)
        {
            int success = 0, error = 0;
            users?.ForEach(u =>
            {
                if (delete(u)) success++; else error++;
            });

            return (success, error);
        }
        #endregion Delete Methods

    }
}
