﻿using HealPassGenerator.Helpers;
using HealPassGenerator.Manager;
using HealPassGenerator.Models;
using HealPassGenerator.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;

namespace HealPassGenerator.ViewModel
{
    public class HealphPassMainViewModel : BaseViewModel
    {
        #region Fields
        private User _selectedUser;
        private ObservableCollection<User> _users;
        private ObservableCollection<int> _progress;
        private int _progessInt;
        private readonly IUserManager userManager;
        private int size;
        private MainView windowHP;
        #endregion Fields

        #region Property
        public string numberOfUsers { get; set; }
        public ICommand WatchCommand { get; }
        public ICommand GeneratechCommand { get; }
        public ICommand GenerateUserHP { get; }
        public ICommand GetAllUserHP { get; }
        public User SelectedUser
        {
            get { return _selectedUser; }
            set
            {
                _selectedUser = value;
                NotifyPropertyChanged("SelectedUser");
            }
        }

        public int ProgressInt
        {
            get { return _progessInt; }
            set
            {
                _progessInt = value;
                NotifyPropertyChanged("ProgressInt");
            }
        }

        public ObservableCollection<User> Users
        {
            get { return _users; }
            set
            {
                _users = value;
                NotifyPropertyChanged("Users");
            }
        }

        public ObservableCollection<int> Progess
        {
            get { return _progress; }
            set
            {
                _progress = value;
                NotifyPropertyChanged("GetProgress");
            }
        }

        #endregion Property

        #region Constructor
        public HealphPassMainViewModel()
        {
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            _users = new ObservableCollection<User>();
            _progress = new ObservableCollection<int>();
            userManager = new UserManager();
            WatchCommand = new RelayCommand(o => CanWatch());
            GeneratechCommand = new RelayCommand(o => Generate());
            GenerateUserHP = new RelayCommand(o => getHealphPass());
            GetAllUserHP = new RelayCommand(o => getAll());

        }
        #endregion Constructor

        #region Methods

        private async void getAll()
        {
            ProgressInt = 0;
           /* userManager.generateAllAsyn((value =>
            {
                if (value.Item1 == 0) ProgressInt = 0;
                else ProgressInt += value.Item1;
                windowHP.UpdateUserStateAndDisplayMessage(ProgressInt, _users,value.Item2, true);
            }));*/
             await userManager.generateAllAsyncPC(value =>
              {
                  if (value.Item1 == 0) ProgressInt = 0;
                  else ProgressInt += value.Item1;
                  windowHP.UpdateUserStateAndDisplayMessage(ProgressInt, _users,value.Item2, true);

              });


        }

        private void getHealphPass()
        {
            if (SelectedUser is null) MessageBox.Show("Please select a user before");
            else
            {
               ProgressInt = 0;
               windowHP.UpdateUserStateAndDisplayMessage(ProgressInt,_users, _selectedUser);
               userManager.generateHPPdf(SelectedUser, value =>
               {
                   if (value == 0) ProgressInt = 0;
                   else ProgressInt += value;

               });

               windowHP.UpdateUserStateAndDisplayMessage(ProgressInt, _users, _selectedUser);

            }
        }

        private void CanWatch()
        {
            if (size == 0) MessageBox.Show("Please generate users before");
            else
            {

               windowHP = new MainView(this);
               Window windowMain = new MainWindow();
               windowHP.Show();
               windowMain.Close();
            }
        }

        private void Generate()
        {
            if(String.IsNullOrWhiteSpace(numberOfUsers))
            {
                MessageBox.Show("Please enter the number of users before");
                return;
            }
            (bool check, int _size) = IsTextAllowed(numberOfUsers);
            if (!check)
            {
                MessageBox.Show("Please enter good number");
                return;
            }
            size = _size;
            Initialize(userManager.Create(size));
            MessageBox.Show("Generate Success");
          
            
        }

        #endregion Methods

        #region Helpers

        private void Initialize(List<User> users)
        {
            if(size > 0) users.Where(u => u != default).ToList().ForEach(u => Users.Add(u));
        }
        private static (bool check, int result) IsTextAllowed(string text)
        {
            Regex _regex = new Regex("[^0-9.-]+");
            (bool check, int result) = IsValid(text);
            return ((!_regex.IsMatch(text) && check), result);
        }

        public static (bool check, int result) IsValid(string str)
        {
            int i;
            return ((int.TryParse(str, out i) && i > 0 && i <= 1000000), i);
        }
        #endregion Helpers




    }
}
