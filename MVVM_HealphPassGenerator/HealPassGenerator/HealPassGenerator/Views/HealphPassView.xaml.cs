﻿using HealPassGenerator.Manager;
using HealPassGenerator.Models;
using HealPassGenerator.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace HealPassGenerator.Views
{
    /// <summary>
    /// Logique d'interaction pour MainView.xaml
    /// </summary>
    public partial class MainView : Window
    {
        public MainView(HealphPassMainViewModel healphPassMainViewModel)
        {
            InitializeComponent();
            DataContext = healphPassMainViewModel;
        }

        public void UpdateUserStateAndDisplayMessage(int progress,ObservableCollection<User> _users, User _selectUser, bool all = false)
        {
            Application.Current.Dispatcher.BeginInvoke(
                DispatcherPriority.Background,
                new Action(() =>
                {
                    _selectUser.HealphPass.downloadState = (progress >= 100 || (all && progress%_users.Count == 0)) ? DownloadState.Download : DownloadState.Progress;
                    (User u, int index) = (_selectUser, _users.IndexOf(_selectUser));
                    _users.Remove(_selectUser);
                    _users.Insert(index, u);
                    if (all && _users.All(u => u.HealphPass.downloadState == DownloadState.Download))
                    {
                        MessageBox.Show("All Health Pass Generate Success");
                    }
                    if (!all && progress >= 100) MessageBox.Show("Generate success");
                }));
        }

        public void CreateDynamiqueProgessBar(int progess, ObservableCollection<User> _users, User _selectUser = default, bool all = false)
        {
            Application.Current.Dispatcher.BeginInvoke(
                DispatcherPriority.Background,
                new Action(() =>
            { 
                    MyprogressBar.IsIndeterminate = false;
                    MyprogressBar.Orientation = Orientation.Horizontal;
                    MyprogressBar.Width = 100;
                    MyprogressBar.Height = 15;
                    if (MyprogressBar.Value >= 100) MyprogressBar.Value = 0;
                    MyprogressBar.Value += !all ? progess : progess >= 100 ? 100/_users.Count : progess/_users.Count;
                    Duration duration = new Duration(TimeSpan.FromSeconds(2));
                    DoubleAnimation doubleanimation = new DoubleAnimation(MyprogressBar.Value, duration);
                    _selectUser.HealphPass.downloadState = progess >= 100 ? DownloadState.Download : DownloadState.Progress;
                    (User u, int index) = (_selectUser, _users.IndexOf(_selectUser));
                    _users.Remove(_selectUser);
                    _users.Insert(index, u);
                    if(all && _users.All(u => u.HealphPass.downloadState == DownloadState.Download))
                    {
                        MyprogressBar.Value = 100;
                        MessageBox.Show("All Health Pass Generate Success");
                    }
                    if(!all && progess >= 100) MessageBox.Show("Generate success");
            }));
        }
    }
}
