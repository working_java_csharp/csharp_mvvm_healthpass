﻿using HealPassGenerator.Models;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using System;
using System.IO;

namespace HealPassGenerator.Helpers
{
    class HealphPassPDFGeHelper
    {
        public void generate(User user, ref int progess)
        {
            // Create PDF Document
            PdfDocument document = new PdfDocument();
            progess += 7;
            // Add page in PDF Document
            PdfPage page = document.AddPage();
            progess += 7;
            // Create XGraphics object
            XGraphics gfx = XGraphics.FromPdfPage(page);
            progess += 7;
            // Font
            XFont fontStateMessage = new XFont("Verdana", 20, XFontStyle.Bold);
            progess += 7;
            XFont fontDate = new XFont("Verdana", 12, XFontStyle.Regular);
            progess += 7;

            // Drawing text on pdf
            gfx.DrawString($"Firstname : {user.Firstname} ", fontStateMessage, XBrushes.Black,
            new XRect(0, -300, page.Width, page.Height), XStringFormats.Center);
            progess += 7;

            gfx.DrawString($"Lastname : {user.Lastname} ", fontStateMessage, XBrushes.Black,
            new XRect(0, -270, page.Width, page.Height), XStringFormats.Center);
            progess += 7;

            gfx.DrawString($"Birthday : {user.BirthDay} ", fontStateMessage, XBrushes.Black,
            new XRect(0, -240, page.Width, page.Height), XStringFormats.Center);
            progess += 7;

            gfx.DrawString($"Status : {user.HealphPass.status} ", fontStateMessage, XBrushes.Black,
            new XRect(0, -200, page.Width, page.Height), XStringFormats.Center);
            progess += 7;

            gfx.DrawString(user.HealphPass.GeneratetDate, fontDate, XBrushes.Black,
            new XRect(20, 20, page.Width, page.Height), XStringFormats.Center);
            progess += 7;
            //Specify file name of the PDF file
            string filename = $"../../../../Doc_HP/{user.Firstname}_{user.Lastname}_{user.Id}.pdf";
            progess += 7;

            try
            {
                var fileStream = new FileStream(filename, FileMode.CreateNew);
                progess += 7;
                document.Save(fileStream);
                progess += 7;
            }
            catch 
            {
                // Log pour informer concernant la génération
                // Ou envoyer un compteur success et error
            }


        }
    }
}
