﻿using HealPassGenerator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealPassGenerator.Helpers
{
    public static class UserHelper
    {
        public static string  generateKey(User user) 
        {
            return $"{user.Firstname}_{user.Lastname}_{user.BirthDay}/{user.Id}";
        }

        public static string generateKey(string Firstname, string Lastname, string BirthDay, int id)
        => $"{Firstname}_{Lastname}_{BirthDay}/{id}";
       

        public static CovidStatus generateCovidStatus()
        {
            Random random = new Random();
            return random.Next(0, 2) == 0 ? CovidStatus.Positif : CovidStatus.Negatif;
        }

        public static int getFielNull()
        {
            Random random = new Random();
            return random.Next(0, 4);
        }
    }
}
