﻿using ConcurrentPriorityQueue.Core;
using HealPassGenerator.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealPassGenerator.Helpers
{
    public class ProducerConsumer
    {
        private readonly ConcurrentPriorityQueue<IHavePriority<int>, int> _concurrentPriorityQueue;
        public ProducerConsumer() =>
            _concurrentPriorityQueue = new ConcurrentPriorityQueue<IHavePriority<int>, int>();
        


        public Task produce(List<User> users)
        {
            return Task.Run(() =>
            {
                Parallel.ForEach(users, user => _concurrentPriorityQueue.Enqueue(user));
            });
        }

        public Task consume(Action<User> action)
        {
            return Task.Run(() =>
            {
                Parallel.For(0, _concurrentPriorityQueue.Count, i =>
                {
                    User user = _concurrentPriorityQueue.Dequeue().Value as User;
                    action.Invoke(user);
                    //Debug.WriteLine($"status : {user.HealphPass.status} priority : {user.Priority}");
                });
            });
        }

        
    }
}
