﻿using HealPassGenerator.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealPassGenerator.Helpers
{
    public static class UserFactory
    {

        #region Create
        public static User Create()
        {
            return new User()
            {
                Id =  Faker.RandomNumber.Next(), // Simuler une clé unique comme en BDD
                Firstname = Faker.Name.First(),
                Lastname = Faker.Name.Last(),
                BirthDay = Faker.Identification.DateOfBirth().ToString("dd/MM/yyyy"),
                HealphPass = new HealphPass()
                {
                    RequestDate = DateTime.Now.ToString(),
                    status = UserHelper.generateCovidStatus(),
                    downloadState = DownloadState.None
                }
            };
        }

        public static User CreateWithFielNull()
        {
            var field = UserHelper.getFielNull();
            return new User()
            {
                Firstname = field == 0 ? default : Faker.Name.First(),
                Lastname = field == 1 ? default : Faker.Name.Last(),
                BirthDay = field == 2 ? default : Faker.Identification.DateOfBirth().ToString(),
                HealphPass = field == 3 ? default : new HealphPass()
                {
                    RequestDate = DateTime.Now.ToString(),
                    status = UserHelper.generateCovidStatus(),
                    downloadState=DownloadState.None
                }
            };
        }

        public static List<User> CreateList(int size = 100)
        {
            var result = new List<User>();
            for(int i = 0; i < size; i++)
            {
                result.Add(Create());
            }

            return result;
        }

        public static List<User> CreateListWithFieldNull(int size = 100, int elemNullFiel = 20)
        {
            var result = new List<User>();
            for (int i = 0; i < (size - elemNullFiel); i++)
            {
                result.Add(Create());
            }

            for (int i = 0; i < elemNullFiel; i++)
            {
                result.Add(CreateWithFielNull());
            }

            return result;
        }

        #endregion Create
    }
}
