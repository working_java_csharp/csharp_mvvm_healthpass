using HealPassGenerator.Helpers;
using HealPassGenerator.Manager;
using HealPassGenerator.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace TestsHealphPass
{
    [TestClass]
    public class UserManagerTest
    {

        #region Create
        [TestMethod]
        public void Create1()
        {
            User user = UserFactory.Create();
            IUserManager userManager = new UserManager();
            Assert.IsTrue(userManager.Create(user));

        }

        [TestMethod]
        public void Create2()
        {
            User user = UserFactory.CreateWithFielNull();
            IUserManager userManager = new UserManager();
            Assert.IsFalse(userManager.Create(user));
        }

        [TestMethod]
        public void Create3()
        {
            User user = default;
            IUserManager userManager = new UserManager();
            Assert.IsFalse(userManager.Create(user));
        }

        [TestMethod]
        public void Create4()
        {
            User user1 = UserFactory.Create(); 
            User user2 = user1;
            IUserManager userManager = new UserManager();
            Assert.IsFalse(userManager.Create(user1) && userManager.Create(user2));
        }

        [TestMethod]
        public void CreateMany()
        {
            List<User> users = UserFactory.CreateList();
            IUserManager userManager = new UserManager();
            (int success, int error) = userManager.Create(users);
            Assert.AreEqual(100, userManager.Count());
            Assert.AreEqual(100, success);
            Assert.AreEqual(0, error);
        }

        [TestMethod]
        public void CreateManyWithErrorUser()
        {
            List<User> users = UserFactory.CreateListWithFieldNull();
            IUserManager userManager = new UserManager();
            (int success, int error) = userManager.Create(users);
            Assert.AreEqual(80, success);
            Assert.AreEqual(20, error);
            Assert.AreEqual(80, userManager.Count());

        }

        [TestMethod]
        public void CreateManyTaskSafe()
        {
            IUserManager userManager = new UserManager();
            var threadCount = 100;

            List<Task> threads = new List<Task>();

            for(int i =  0; i < threadCount; i++)
            {
                var thread = Task.Run(() =>
                {
                    var user = UserFactory.Create();
                    userManager.Create(user);
                });

                threads.Add(thread);
            }
            Task.WaitAll(threads.ToArray());

            Assert.AreEqual(100, userManager.Count());

        }

        [TestMethod]
        public void CreateManyThreadSafe2()
        {
            IUserManager userManager = new UserManager();
            var threadCount = 100;

            List<Task> threads = new List<Task>();

            for (int i = 0; i < threadCount; i++)
            {
                var thread = Task.Run(() =>
                {
                    for(int j = 0; j < threadCount ; j++)
                    {
                        var user = UserFactory.Create();
                        userManager.Create(user);
                    }
                    
                });
                var thread2 = Task.Run(() =>
                {
                    for (int j = 0; j < 20; j++)
                    {
                        var user = UserFactory.CreateListWithFieldNull();
                        userManager.Create(user);
                    }

                });

                threads.Add(thread);
                threads.Add(thread2);
            }
            Task.WaitAll(threads.ToArray());
            Assert.AreEqual(170000, userManager.Count());

        }

        [TestMethod]
        public void CreateManyThreadSafe3()
        {
            IUserManager userManager = new UserManager();
            var threadCount = 100;

            List<Task> threads = new List<Task>();

            for (int i = 0; i < threadCount; i++)
            {
                var thread = Task.Run(() =>
                {
                    for (int j = 0; j < threadCount; j++)
                    {
                        var user = UserFactory.CreateList(); 
                        userManager.Create(user);
                    }

                });

                threads.Add(thread);
            }
            Task.WaitAll(threads.ToArray());
            Assert.AreEqual(1000000, userManager.Count());

        }

        [TestMethod]
        public void CreateManyThreadSafe4()
        {
            IUserManager userManager = new UserManager();
            var threadCount = 100;
            int success = 0, error = 0;
            List<Task> threads = new List<Task>();

            for (int i = 0; i < threadCount; i++)
            {
                var thread = Task.Run(() =>
                {
                    for (int j = 0; j < threadCount; j++)
                    {
                        var users = UserFactory.CreateListWithFieldNull(); 
                       (int s, int e) = userManager.Create(users);
                        success += s;
                        error += e;
                    }

                });

                threads.Add(thread);
            }
            Task.WaitAll(threads.ToArray());
            Assert.AreEqual(800000, userManager.Count());

        }

        #endregion Create
        #region Find
        [TestMethod]
        public void find()
        {
            User user = UserFactory.Create();
            IUserManager userManager = new UserManager();
            userManager.Create(user);
            Assert.AreEqual(userManager.find(user), user);
        }

        [TestMethod]
        public void find2()
        {
            User user = default;
            IUserManager userManager = new UserManager();
            Assert.AreEqual(userManager.find(user), default);
        }

        [TestMethod]
        public void find3()
        {
            User user = UserFactory.Create();
            User user2 = UserFactory.Create();
            IUserManager userManager = new UserManager();
            userManager.Create(user);
            userManager.Create(user2);
            Assert.AreEqual(userManager.find(user), user);
            Assert.AreEqual(userManager.find(user2), user2);
        }

        [TestMethod]
        public void find4()
        {
            User user = UserFactory.Create();
            User user2 = default;
            IUserManager userManager = new UserManager();
            userManager.Create(user);
            userManager.Create(user2);
            Assert.AreEqual(userManager.find(user), user);
            Assert.AreEqual(userManager.find(user2), default);
        }

        [TestMethod]
        public void findMany()
        {
            List<User> users = UserFactory.CreateList();
            IUserManager userManager = new UserManager();
            userManager.Create(users);
            Assert.AreEqual(users.Count, userManager.find(users).Count);
        }

        [TestMethod]
        public void findMany2()
        {
            List<User> users = UserFactory.CreateListWithFieldNull();
            IUserManager userManager = new UserManager();
            userManager.Create(users);
            Assert.AreEqual(80, userManager.find(users).Count);
        }

        [TestMethod]
        public void findMany3()
        {
            List<User> users = new List<User>();
            IUserManager userManager = new UserManager();
            Assert.AreEqual(0, userManager.find(users).Count);
        }
        [TestMethod]
        public void findMany4()
        {
            List<User> users = default;
            IUserManager userManager = new UserManager();
            Assert.IsNull(userManager.find(users)); ;
        }

        [TestMethod]
        public void findManyWithTread() // Marche 1 fois/4
        {
            List<User> users = UserFactory.CreateList();
            IUserManager userManager = new UserManager();
            (int success, int error) = userManager.Create(users);
            List<Task> threads = new List<Task>();
            List<User> result = new List<User>();
            users.ForEach(u =>
           {
               var thread = Task.Run(() =>
               {
                   result.Add(userManager.find(u));
               });
               threads.Add(thread);
           });
            Task.WaitAll(threads.ToArray());

          Assert.AreEqual(users.Count, result.Count);
        }

        [TestMethod]
        public void findManyWithTread2()
        {
            IUserManager userManager = new UserManager();
            List<Task> threads = new List<Task>();
            List<User> result = new List<User>();
            int threadNumber = 100;
            for(int i = 0; i< threadNumber; i++)
            {
                var user = UserFactory.Create();
                var thread1 = Task.Run(() =>
                {
                    userManager.Create(user);
                });
                var thread2 = Task.Run(() =>
                {
                    result.Add(userManager.find(user));
                });
                threads.Add(thread1);
                threads.Add(thread2);
            }
           
            Task.WaitAll(threads.ToArray());

            Assert.AreEqual(100, result.Count);
        }
        #endregion FInd
        #region Delete
        [TestMethod]
        public void delete()
        {
            User user = UserFactory.Create();
            IUserManager userManager = new UserManager();
            userManager.Create(user);
            Assert.IsTrue(userManager.delete(user));
        }

        [TestMethod]
        public void delete2()
        {
            User user = default;
            IUserManager userManager = new UserManager();
            Assert.AreEqual(userManager.delete(user), default);
        }

        [TestMethod]
        public void delete3()
        {
            User user = UserFactory.Create();
            User user2 = UserFactory.Create();
            IUserManager userManager = new UserManager();
            userManager.Create(user);
            userManager.Create(user2);
            Assert.IsTrue(userManager.delete(user));
            Assert.IsTrue(userManager.delete(user2));
        }

        [TestMethod]
        public void delete4()
        {
            User user = UserFactory.Create();
            User user2 = default;
            IUserManager userManager = new UserManager();
            userManager.Create(user);
            userManager.Create(user2);
            Assert.IsTrue(userManager.delete(user));
            Assert.AreEqual(userManager.delete(user2), default);
        }

        [TestMethod]
        public void deleteMany()
        {
            List<User> users = UserFactory.CreateList();
            IUserManager userManager = new UserManager();
            userManager.Create(users);
            Assert.AreEqual((users.Count,0), userManager.delete(users));
        }

        [TestMethod]
        public void deleteMany2()
        {
            List<User> users = UserFactory.CreateListWithFieldNull();
            IUserManager userManager = new UserManager();
            userManager.Create(users);
            Assert.AreEqual((80,20), userManager.delete(users));
        }

        [TestMethod]
        public void deleteMany3()
        {
            List<User> users = new List<User>();
            IUserManager userManager = new UserManager();
            Assert.AreEqual((0,0), userManager.delete(users));
        }
        [TestMethod]
        public void deleteMany34()
        {
            List<User> users = default;
            IUserManager userManager = new UserManager();
            Assert.AreEqual((0, 0), userManager.delete(users));
        }
        #endregion Delete
    }
}
