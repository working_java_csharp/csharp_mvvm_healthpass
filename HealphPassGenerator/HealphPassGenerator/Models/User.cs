﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealphPassGenerator.Models
{
    public class User : DataBase
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string BirthDay { get; set; }
        public HealthPass HealthPass { get; set; }
    }
}
