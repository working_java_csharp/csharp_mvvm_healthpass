﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealphPassGenerator.Models.DTO
{
    public class UserDTO : IComparable
    {
        
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string BirthDay { get; set; }
        public string RequestDate { get; set; }
        public CovidState State { get; set; }

        public int CompareTo(object obj)
        {
            var objUserDTO = obj as UserDTO;
            if (objUserDTO.State > State) return 1;
            return objUserDTO.State < State ? -1 : 0;
        }

        private static Random rd = new Random();
        public User UserDTOtoUser()
        {

            return new User()
            {
                Firstname = Firstname,
                Lastname = Lastname,
                BirthDay = BirthDay,
                HealthPass = new HealthPass()
                {
                    State = rd.Next(0, 1) == 1 ? CovidState.Positif : CovidState.Negatif,
                    GenerateDate = new DateTime().ToString()
                }
            };
        }
    }
}
