﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealphPassGenerator.Models
{
    public class HealthPass : DataBase
    {
        public string RequestDate { get; set; }
        public string GenerateDate { get; set; }
        public CovidState State { get; set; }
    }
}
