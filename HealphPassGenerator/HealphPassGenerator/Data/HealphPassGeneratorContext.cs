﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using HealphPassGenerator.Models;

namespace HealphPassGenerator.Data
{
    public class HealphPassGeneratorContext : DbContext
    {
        public HealphPassGeneratorContext (DbContextOptions<HealphPassGeneratorContext> options)
            : base(options)
        {
        }

        public DbSet<HealphPassGenerator.Models.User> User { get; set; }
    }
}
