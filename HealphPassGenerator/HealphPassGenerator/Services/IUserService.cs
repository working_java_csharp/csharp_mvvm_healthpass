﻿using HealphPassGenerator.Models;
using HealphPassGenerator.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealphPassGenerator.Services
{
    public interface IUserService
    {
        Task<List<User>> GetAllAsync();
        Task<User> GetByInfo(string firstname, string lastname, string Birthday);
        List<User> GetManyByInfo(List<User> users);
        Task Create(UserDTO userDTO);
        Task Create(List<UserDTO> userDTOs);
        Task<bool> delete(UserDTO userDTO);
    }
}
