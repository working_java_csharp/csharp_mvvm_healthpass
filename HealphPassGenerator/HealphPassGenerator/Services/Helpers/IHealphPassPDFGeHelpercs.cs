﻿using HealphPassGenerator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealphPassGenerator.Services.Helpers
{
    public interface IHealphPassPDFGeHelpercs
    {
        public void generate(User user);
    }
}
