﻿using HealphPassGenerator.Models;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace HealphPassGenerator.Services.Helpers
{
    public class HealphPassPDFGeHelper
        : IHealphPassPDFGeHelpercs
    {
        public void generate(User user)
        {
            // Create PDF Document
            PdfDocument document = new PdfDocument();

            // Add page in PDF Document
            PdfPage page = document.AddPage();
            // Create XGraphics object
            XGraphics gfx = XGraphics.FromPdfPage(page);
            // Font
            XFont fontStateMessage = new XFont("Verdana", 20, XFontStyle.Bold);
            XFont fontDate = new XFont("Verdana", 7, XFontStyle.Regular);
            gfx.DrawString(GetUserStateMessage(user), fontStateMessage, XBrushes.Black,
            new XRect(0, 0, page.Width, page.Height), XStringFormats.Center);
            gfx.DrawString(user.HealthPass.GenerateDate, fontDate, XBrushes.Black,
            new XRect(0, 0, page.Width, page.Height), XStringFormats.Center);
            //Specify file name of the PDF file
            string filename = $"{user.Firstname}_{user.Lastname}.pdf";
            //Save PDF File
            document.Save(filename);
            //Load PDF File for viewing
            Process.Start(filename);
        }


        public string GetUserStateMessage(User user)
        {
            return $"Firstname : {user.Firstname} \n Lastname : {user.Lastname} \n Birthday : {user.BirthDay}" +
                $"\n Status : {user.HealthPass.State.ToString()}";
        }
    }
}
