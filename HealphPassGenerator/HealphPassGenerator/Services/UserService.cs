﻿using HealphPassGenerator.Data;
using HealphPassGenerator.Models;
using HealphPassGenerator.Models.DTO;
using HealphPassGenerator.Services.Helpers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealphPassGenerator.Services
{
    public class UserService 
        : IUserService
    {
        private readonly HealphPassGeneratorContext _context;
        private readonly IHealphPassPDFGeHelpercs _healphPassPDFGeHelpercs;

        public UserService(HealphPassGeneratorContext context, IHealphPassPDFGeHelpercs healphPassPDFGeHelpercs)
        {
            _context = context;
            _healphPassPDFGeHelpercs = healphPassPDFGeHelpercs;
        }

        public async Task<List<User>> GetAllAsync()
        {
           return  await _context.User.ToListAsync();
        }

        public async Task<User> GetByInfo(string firstname, string lastname, string Birthday)
        {
            return await _context.User.FirstOrDefaultAsync(u => (u.Firstname == firstname && u.Lastname == lastname && u.BirthDay == Birthday));
        }

        public  List<User> GetManyByInfo(List<User> users)
        {
            List<User> result = new List<User>();
            users.ForEach(async u => {
                var user = await GetByInfo(u.Firstname, u.Lastname, u.BirthDay);
                result.Add(user);
            
            });

            return result;
        }

        public async Task Create(UserDTO userDTO)
        {
            var user = userDTO.UserDTOtoUser();

            _context.Add(user);
            await _context.SaveChangesAsync();
            _healphPassPDFGeHelpercs.generate(user);
        }

        public async Task Create(List<UserDTO> userDTOs)
        {
            userDTOs.Sort();
            var users = userDTOs.Select(u => u.UserDTOtoUser()).ToList(); 
            _context.AddRange(users);
            await _context.SaveChangesAsync();

            // Générer le passe pour chacun
        }


        public async Task<bool> delete(UserDTO userDTO) 
        {
            var user = await _context.User.FirstOrDefaultAsync(u => (u.Firstname == userDTO.Firstname && u.Lastname == userDTO.Lastname && u.BirthDay == userDTO.BirthDay));
            if (user == default) return false;
            _context.User.Remove(user);
            await _context.SaveChangesAsync();
            return true;
        }


    }
}
